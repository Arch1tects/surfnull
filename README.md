# SurfNull SourceMod Plugin (CS:GO)


SurfNull is a plugin exclusively for Counter-Strike: Global Offensive servers. Here, the Surf game mode is implemented, which includes sliding on inclined surfaces.

## Features

* none

## Usage

### Server Requirements

* 128 Tick (`-tickrate 128`)
* [SourceMod ^1.10](https://www.sourcemod.net/downloads.php?branch=stable)
* Optional - [Updater Plugin](https://forums.alliedmods.net/showthread.php?t=169095) (automatically install minor SurfNull updates)

### Installing

* none

### Author

* **Arhitect** - *Lead Developer* - [GitHub](https://github.com/Arch1tects)